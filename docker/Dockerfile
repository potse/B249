#
# Configuration to create a Docker image that can compile our
# website on the gitlab.inria servers, directly from the source files
# in the repository.
#
# Use:
#
#    docker build -t registry.gitlab.inria.fr/potse/b249 .
#    docker login    registry.gitlab.inria.fr
#    docker push     registry.gitlab.inria.fr/potse/b249
#
# For local testing, mount the root of the project at /builds ; this is
# likely also what happens on the gitlab server. Here also with -i and -t
# to run it interctively, and assuming it is run from the project root:
#
#    docker run -it -v `pwd`:/builds test

# parent image from which we build.
#
FROM ubuntu

# Installing as root: docker images are usually set up as root.
# Since some autotools scripts might complain about this being unsafe, we set
# FORCE_UNSAFE_CONFIGURE=1 to avoid configure errors.
ENV FORCE_UNSAFE_CONFIGURE=1
ENV DEBIAN_FRONTEND noninteractive

# Packages we (may) need. Python 2.7 for sure, but we add a few more
# things in case we are going to do more here later.
#
RUN apt-get update && apt-get install -y \
  sudo \
  bash \
  wget \
  curl \
  patch \
  unzip \
  bzip2 \
  gawk \
  bison \
  flex \
  git \
  subversion \
  mercurial \
  build-essential \
  clang \
  cmake \
  cmake-data\
  cmake-curses-gui \
  graphviz \
  doxygen \
  python2.7 \
  python-pip && \
  apt-get autoremove -y

# epydoc is used to create the Python API documentation
#
RUN pip install --upgrade pip && \
    python -m pip install epydoc

# create our user
#
RUN groupadd -f -g 1000 gitlab && \
    useradd -u 1000 -g gitlab -ms /bin/bash gitlab && \
    mkdir /builds && \
    chown -R gitlab:gitlab /builds && \
    echo "gitlab:gitlab" | chpasswd && adduser gitlab sudo

RUN chown -R gitlab:gitlab /home/gitlab/

# change user
USER gitlab

# change the default shell to be bash
SHELL ["/bin/bash", "-c"]

# Set the working directory. This will be the working directory when
# gitlab-ci runs the image, and it will then fork the repository here,
# so this becomes the root of the repository contents.
# This can be mimicked on the command line with
# docker ... -v `pwd`:/builds
#
WORKDIR /builds
