
## Introduction

This is a fork of the Blender source version 2.49b. It was slightly
modified to keep it compilable over a decade after it was released,
and to introduce some features that we need. The original reason to do
this was that we wanted to use Blender with Python 2.7, while all
available binary distributions were compiled with python 2.5. Some
changes in the CMake files had to be made to adapt to modern
systems. A few other things were modified subsequently, just to fit
our needs.

The original Blender program is described on http://www.blender.org
Source code and binary distributions are also downloadable there.
There is an achlinux script for it on https://aur.archlinux.org/packages/blender249/


### Installation

First download the source code by cloning this repository.

The original installation instructions that came with Blender 2.49 are
in the file INSTALL. You should probably ignore it; it does not even
mention cmake. Up-to-date installation instructions are given below.

```
 mkdir build
 cd build
 cmake -DCMAKE_EXE_LINKER_FLAGS="-lX11 -ldl" -DWITH_OPENAL:BOOL=OFF -DWITH_OPENEXR:BOOL=OFF ../
 make

 Then install the file build/bin/blender where you would like to have it.

Useful cmake options:

 -DWITH_OPENAL:BOOL=OFF  disables OpenAL (sound)

 -DWITH_OPENEXR:BOOL=OFF  disables OpenEXR (an image file format)

 -DCMAKE_BUILD_TYPE:STRING="Debug"  makes a debug version (CC -g and no optimization)

 -DCMAKE_EXE_LINKER_FLAGS  : options to pass to the linker
```


### Thanks

  - the Blender community for making this wonderful beast

  - Simone Pezzuto for adapting the CMake files



### User Documentation

We have our own version of the Python API documentation online at
https://potse.gitlabpages.inria.fr/B249/


### Inventory
## Specifics for B249

 * `docker` : preparation of a Docker image for construction and
   publication of the website


## Inherited from Blender 2.49b

There's not much information about what each source file does or is
supposed to do. Here are a few bits we found out.


 * `source/blender/blenkernel/intern` : much of the core code of
   Blender, for example the construction of "display lists" for
   various objects from their basic parameters.

 * `source/blender/render/intern/source/convertblender.c` : Conversion
   of object (mesh,curve, ...) data for the renderer; e.g. data
   format conversion and calculation of normals.

   
 * `source/blender/python/api2_2x/` : Python API

 * `source/blender/python/api2_2x/doc` : Python API
   documentation. There is a script `epy_docgen.sh` to generate HTML
   documentation from the Python headers of the API.
   The gitlab repository has a Pipelin that runs this script on the
   repository version of the files and publishes them; this is defined
   in `.gitlab-ci.yml`.

   
 * `source/blender/render/intern/source/texture.c` : application of textures
 

 * `source/blender/src/buttons_*.c` : GUI (Buttons Window)


 * `source/blender/blenkernel/intern/curve.c` : treatment of Curve objects:
     create, copy, free, create a bevel, ... Application of a bevel to a
     curve is done in `displist.c`


 * `projectfiles*` : configuration for Microsoft compilers ; obviously we
     don't maintain this
 